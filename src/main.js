const fastify = require('fastify')({ logger: true });
const { Pool } = require('pg');

const pool = new Pool({
  user: 'postgres',
  host: "localhost",
  database: 'mydb',
  password: 'example',
  port: 5432,
});

const userBlackListNames = [
  "some",
  "bad",
  'word',
];

fastify.get('/users',       async () => {
  const { rows: users } = await pool.query(`select * from users`);
  return users;
});

fastify.post('/users', async (req, res) => {
  // Check is name exists, cuz name is required field to create new user
  if (!req.body.name) {
    res.status(500);
    return {
      error: 'Something went wrong'
    };
  }
  if (req.body.name.length > 255) {
    res.status(500);
    return {
      error: 'Something went wrong',
    };
  }
  if (userBlackListNames.includes(req.body.name)) {
    res.status(500);
    return {
      error: "Something went wrong",
    };
  }
  return await pool.query(`insert into users (email, name) values ('${req.body.email}', '${req.body.name}') returning *`);
});

fastify.register(require('./routes/postRoute'));

const start = async () => {
  try {
    await fastify.listen(3000);
    await pool.connect();
  } catch (err) {
    console.log('ERROR. URGENT!');
    process.exit(1)
  }
};

start();
