async function routes(fastify, options) {
  fastify.get('/getPosts', async () => {
  });
  fastify.post('/createPost', async () => {

  });
  fastify.get('/getOnePost', async () => {

  });
  fastify.patch('/updatePost', async () => {

  });
  fastify.delete('/deletePost', async () => {

  });
}

module.exports = routes
